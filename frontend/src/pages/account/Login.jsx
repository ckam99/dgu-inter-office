import React, { useState } from "react";
import Layout from "../../layouts/Auth";
import logo from "../../assets/svg/logo.svg";
import auth from "../../api/auth";
import Button from "../../components/elements/Button";
import { useFormik } from "formik";
import * as Yup from "yup";
import Page from "../../components/shared/Page";
import { navigate } from "@reach/router";

export default (props) => {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const validateSchema = Yup.object({
    email: Yup.string()
      .email("Invalid email")
      .required("This field is required"),
    password: Yup.string()
      .required("This field is required")
      .min(4, "At least 4 characters"),
  });
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validateSchema,
    onSubmit: (values) => {
      setLoading(true);
      auth.login(values).then((response) => {
        setLoading(false);
        if (response.error) {
          setError(true);
        } else {
          navigate("/");
        }
      });
    },
  });

  return (
    <Layout>
      <Page title="DGU:::LOGIN" transition="fade-in">
        <div className="form-content">
          <form className="form" onSubmit={formik.handleSubmit}>
            <div
              className="flex flex-column flex-align-center"
              style={{ marginBottom: "10px" }}
            >
              <img src={logo} alt="" width="150" height="150" />
            </div>
            <div className="flex flex-justify-content-center">
              <span
                className="smart-text"
                style={{
                  fontSize: "24px",
                  color: "#4F4F4F",
                  marginBottom: "10px",
                  fontWeight: "bold",
                }}
              >
                Вход
              </span>
            </div>

            {error && (
              <div className="alert alert-error ">
                <span>неверный адрес электронной почты или пароль</span>
              </div>
            )}
            <div>
              <input
                className="text-field"
                type="email"
                name="email"
                placeholder="Email"
                style={{ marginBottom: "10px", marginTop: "10px" }}
                value={formik.values.email}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
              />
              {formik.errors.email && formik.touched.email && (
                <small className="error-text">{formik.errors.email}</small>
              )}
              <input
                className="text-field"
                type="password"
                name="password"
                placeholder="Password"
                style={{ marginBottom: "10px" }}
                value={formik.values.password}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
              />
              {formik.errors.password && formik.touched.password && (
                <small className="error-text">{formik.errors.password}</small>
              )}
              <div align="right">
                <a href="/account/forgotpassword" className="smrt-link">
                  Забыли пароль?
                </a>
              </div>
              <Button className="flat-btn" type="submit" loading={loading}>
                Войти
              </Button>
            </div>
          </form>
        </div>
      </Page>
    </Layout>
  );
};
