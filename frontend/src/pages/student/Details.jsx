import React, { useState, useEffect, useLayoutEffect } from "react";
import Layout from "../../layouts/Main";
import StudentOverview from "../../components/student/StudentOverview";
import StudentPassport from "../../components/student/StudentPassport";
import StudentVisa from "../../components/student/StudentVisa";
import StudentCourse from "../../components/student/StudentCourse";
import Page from "../../components/shared/Page";
import Student from "../../api/student";
import StudentCorrespondant from "../../components/student/StudentCorrespondant";
import StudentHistory from "../../components/student/StudentHistory";

export default ({ studentId }) => {
  const [registers, setRegisters] = useState([]);
  const [register, setRegister] = useState(null);
  const [visas, setVisas] = useState([]);
  const [student, setStudent] = useState(null);
  const [correspondants, setCorrespondants] = useState([]);
  const [passports, setPassports] = useState([]);
  const [passport, setPassport] = useState(null);

  useEffect(() => {
    Student.getById(studentId).then((response) => setStudent(response.data));
    Student.getVisas(studentId).then((response) => setVisas(response.data));
    Student.getRegisters(studentId).then((response) =>
      setRegisters(response.data)
    );
    Student.getCorrespondants(studentId).then((response) =>
      setCorrespondants(response.data)
    );
    Student.getPassports(studentId).then((response) =>
      setPassports(response.data)
    );
  }, [studentId]);

  useLayoutEffect(() => {
    if (registers.length > 0) {
      setRegister(registers[registers.length - 1]);
    }
    if (passports.length > 0) {
      setPassport(passports[passports.length - 1]);
    }
  }, [registers, passports]);
  return (
    <Layout>
      <Page title="Student list ...">
        <div className="flex">
          <section className=" flex-1">
            {/* School programs */}
            <StudentCourse registers={registers} />
            <StudentVisa visas={visas} />
            <StudentCorrespondant correspondants={correspondants} />
            <StudentHistory />
          </section>

          {/* right bloc */}
          <section>
            {/* Portrait */}
            <StudentOverview student={student} register={register} />
            <StudentPassport passport={passport} />
          </section>
        </div>
      </Page>
    </Layout>
  );
};
