import React from "react";
import Layout from "../../layouts/Main";
import StudentList from "../../components/student/StudentList";
import SearchField from "../../components/elements/SearchField";
import Student from "../../api/student";
import Page from "../../components/shared/Page";
import Paginator from "../../components/shared/Paginator";
import { navigate } from "@reach/router";
import { useRecoilValue } from "recoil";
import { currentScholarshipState } from "../../store/states/scholarship";

export default () => {
  const scholarship = useRecoilValue(currentScholarshipState);

  const [students, setStudents] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [pageSize, setPageSize] = React.useState(10);
  const [dataCount, setDataCount] = React.useState(0);
  const [searchItem, setSearchItem] = React.useState("");
  let limit = 10;
  let offset = 0;

  React.useEffect(() => {
    loadData(searchItem);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scholarship]);

  function loadData(search) {
    setLoading(true);
    Student.getCurrentRegisters(scholarship, limit, offset, search).then(
      (response) => {
        if (!response.error) {
          setStudents(response.data.result);
          setDataCount(response.data.count);
        }
        setLoading(false);
      }
    );
  }

  function onSearchChange(e) {
    loadData(e);
    setSearchItem(e);
  }

  function onPageSizeChange(e) {
    setPageSize(e.target.value);
    const newSize = parseInt(e.target.value);
    limit = newSize;
    offset = newSize - newSize;
    loadData(searchItem);
  }

  function onPaginate(page) {
    limit = pageSize * page;
    offset = pageSize * page - pageSize;
    loadData(searchItem);
  }

  function onStuentSelected(id) {
    navigate(`/student/${id}`);
  }

  return (
    <Layout title="Student list ...">
      <Page>
        <div className="panel">
          <div className="panel-body">
            <div className="flex align-items-center flex-justify-content-space-between">
              <SearchField
                placeholder="Поиск"
                width="350"
                onChange={onSearchChange}
              />
              <button
                className="btn btn-default"
                style={{ padding: "7px 30px" }}
              >
                Создать
              </button>
            </div>
          </div>
        </div>
        <div className="panel">
          <div className="panel-body">
            <StudentList
              students={students}
              onSelected={onStuentSelected}
              loading={loading}
            />
            {dataCount > pageSize && (
              <nav className="paginator">
                <Paginator
                  totalLines={dataCount}
                  linesPerPage={pageSize}
                  textColor="var(--primary-color)"
                  paginate={onPaginate}
                />
                <div className="paginator-lignes">
                  <span>данных на страницу</span>
                  <select
                    className="text-field"
                    value={pageSize}
                    onChange={onPageSizeChange}
                  >
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                  </select>
                </div>
              </nav>
            )}
          </div>
        </div>
      </Page>
    </Layout>
  );
};
