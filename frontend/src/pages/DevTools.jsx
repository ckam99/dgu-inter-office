import React, { useRef, useState, useEffect } from "react";
import Layout from "../layouts/Main";
import Modal from "../components/shared/Modal";
import Popup from "../components/shared/Popup";
import DatePicker, { registerLocale } from "react-datepicker";
import ru from "date-fns/locale/ru";
import "react-datepicker/dist/react-datepicker.css";

import ReactCountryFlag from "react-country-flag";
import Button from "../components/elements/Button";
import Page from "../components/shared/Page";
import country from "../api/country";

registerLocale("ru", ru);

export default () => {
  const modal = useRef(null);
  const [date, setDate] = useState(null);
  const popup = useRef(null);
  const [loading, setLoading] = useState(false);
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    country.getAll().then((response) => {
      setCountries(response.data);
    });
  }, []);

  const openPopup = () => {
    popup.current.open();
  };

  const openModal = () => {
    modal.current.open();
  };

  const handleClick = () => {
    setLoading((l) => (l = true));
  };

  return (
    <Layout title="devtools ...">
      <Page transition="slide-right">
        <div className="grid g-col-2">
          <div className="flex flex-column ">
            <div className="row">
              <h1>DevTools</h1>
              <Button onClick={handleClick} loading={loading}>
                Loader button
              </Button>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quam,
                nobis repudiandae. Voluptatum a placeat ad cumque. Minima neque
                aperiam, voluptas itaque incidunt, obcaecati cumque quae hic
                harum, eveniet facere repellat?
              </p>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quam,
                nobis repudiandae. Voluptatum a placeat ad cumque. Minima neque
                aperiam, voluptas itaque incidunt, obcaecati cumque quae hic
                harum, eveniet facere repellat?
              </p>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quam,
                nobis repudiandae. Voluptatum a placeat ad cumque. Minima neque
                aperiam, voluptas itaque incidunt, obcaecati cumque quae hic
                harum, eveniet facere repellat?
              </p>
            </div>
            <div className="row">
              <button onClick={openPopup}>open popup</button>
              <button onClick={openModal}>open modal</button>
            </div>
          </div>

          <div className="row">
            <h1>Welcome DGU INTER</h1>
            <div className="input-group inline">
              <label htmlFor="">Name</label>
              <input type="text" className="text-field" />
            </div>

            <div className="input-group inline">
              <label htmlFor="">Date picker</label>
              <input type="date" className="text-field" />
              <input type="time" className="text-field" />
            </div>

            <div className="input-group">
              <label htmlFor="">Date</label>
              <DatePicker
                className="text-field"
                showPopperArrow={false}
                selected={date}
                onChange={(date) => setDate(date)}
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                timeCaption="time"
                dateFormat="d.MM.yyyy  hh:mm  aa"
                placeholderText="Select date"
                locale="ru"
              />
            </div>

            <label htmlFor="" className="check-group">
              <input type="checkbox" name="" id="" />
              checkbox
            </label>

            <label htmlFor="r" className="check-group">
              <input type="radio" name="" id="r" />
              radio button
            </label>

            <div className="input-group">
              <label htmlFor="">Name</label>
              <select className="text-field">
                <option value="">a</option>
                <option value="">b</option>
                <option value="">c</option>
                <option value="">d</option>
              </select>
            </div>

            <label htmlFor="s1" className="check-group">
              <input id="s1" type="checkbox" className="switch" />
              Switch
            </label>

            <br />
            <button
              onClick={() => {
                modal.current.open();
              }}
            >
              open modal
            </button>
          </div>

          <div className="row">
            <div>
              <h1>Emoji's</h1>
              <div style={{ display: "flex", flexFlow: "row wrap" }}>
                {countries.map((country) => (
                  <div
                    style={{ margin: "10px", cursor: "pointer" }}
                    title={country.name}
                    key={`${country.iso2}_${country.e164}_emoji`}
                  >
                    <ReactCountryFlag
                      countryCode={country.iso2}
                      style={{
                        fontSize: "3em",
                      }}
                    />
                  </div>
                ))}
              </div>
              <br />
            </div>
          </div>
          <div className="row">
            <h1>SVG's</h1>
            <div style={{ display: "flex", flexFlow: "row wrap" }}>
              {countries.map((country, i) => (
                <div
                  key={`${country.iso2}_${country.e164}_emoji`}
                  style={{ margin: "10px", cursor: "pointer" }}
                  title={country.name}
                >
                  <ReactCountryFlag
                    countryCode={country.iso2}
                    svg
                    style={{
                      width: "3em",
                      height: "3em",
                    }}
                  />
                </div>
              ))}
            </div>
            <br />
          </div>
        </div>

        <Modal title="ghghg" refId={modal}>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe alias
            commodi assumenda impedit, illo placeat facilis repudiandae sequi ex
            quaerat perspiciatis excepturi et, odit doloribus quam similique
            numquam? Necessitatibus, ab.
          </p>
          <button
            onClick={() => {
              modal.current.close();
            }}
          >
            Close
          </button>
        </Modal>
        <Popup title="Add new student" refId={popup}>
          <h2>Bloc parcel</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et
            voluptatum deleniti, minima placeat magnam distinctio a alias ipsa
            iure, quisquam esse sapiente exercitationem, ut repudiandae.
            Voluptatum, possimus? Molestias, minima amet.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et
            voluptatum deleniti, minima placeat magnam distinctio a alias ipsa
            iure, quisquam esse sapiente exercitationem, ut repudiandae.
            Voluptatum, possimus? Molestias, minima amet.
          </p>
          <button
            onClick={() => {
              popup.current.close();
            }}
          >
            Close
          </button>
        </Popup>
      </Page>
    </Layout>
  );
};
