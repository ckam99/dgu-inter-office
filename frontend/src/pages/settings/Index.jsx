import React from "react";
import Layout from "../../layouts/Main";
import Page from "../../components/shared/Page";

export default () => {
  return (
    <Layout title="Settings ...">
      <Page>
        <h2>Settings...</h2>
      </Page>
    </Layout>
  );
};
