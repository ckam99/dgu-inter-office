import React from "react";
import Layout from "../layouts/Main";
import Page from "../components/shared/Page";

export default () => {
  return (
    <Layout title="Welcome ..." breadcrumbs={["Darshbord"]}>
      <Page>
        <h2>Home page</h2>
        <div className="row flex flex-align-center flex-justify-content-center pt-10"></div>
      </Page>
    </Layout>
  );
};
