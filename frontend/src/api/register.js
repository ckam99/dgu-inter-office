import api from "./index";

class Register {
  static async getAll(scholarship = null) {
    try {
      let params = {};
      if (scholarship !== null) {
        params = { ...params, scholarship };
      }
      const response = await api.get(`/registers/`);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async getByStudent(student, scholarship = null) {
    try {
      let params = {
        student: student,
      };
      if (scholarship !== null) {
        params = { ...params, scholarship };
      }
      const response = await api.get(`/registers/`, { params });
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async create(credentials) {
    try {
      const res = await api.post(`/registers/`, credentials);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async update(credentials, id) {
    try {
      const res = await api.put(`/registers/${id}/`, credentials);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async delete(id) {
    try {
      const res = await api.delete(`/registers/${id}/`);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }
}

export default Register;
