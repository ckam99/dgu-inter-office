import api from "./";

class Country {
  async getAll() {
    try {
      const response = await api.get(`/countries/`);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async getById(id) {
    try {
      const response = await api.get(`/countries/${id}/`);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }
}

export default new Country();
