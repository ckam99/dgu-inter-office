import { atom } from "recoil";
import storage from "../storage";

export const currentScholarshipState = atom({
  key: "current-scholarship-state",
  default: storage.get("scholarship"),
});
