import { atom } from "recoil";

export const hideSidebarState = atom({
  key: "sidebar-state",
  default: false,
});
