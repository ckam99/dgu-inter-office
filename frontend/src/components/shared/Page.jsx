import React, { useEffect } from "react";

const Page = ({ children, title, transition = "fade-in" }) => {
  useEffect(() => {
    document.title = title || "DGU INTERNATIONAL OFFICE";
  });

  return (
    <div
      className="page"
      style={{
        position: "relative",
        width: "100%",
        height: "100%",
        transition: "all 0.8s ease",
        animation: `${transition} 0.8s ease-in-out forwards`,
      }}
    >
      {children}
    </div>
  );
};

export default Page;
