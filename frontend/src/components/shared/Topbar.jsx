import React from "react";
import Icon from "../elements/Icon";
import Dropdown from "../elements/Dropdown";
import Crop from "../elements/Crop";
import authApi from "../../api/auth";
import scholarshipApi from "../../api/scholarship";
import { navigate } from "@reach/router";
import { useRecoilState } from "recoil";
import { currentScholarshipState } from "../../store/states/scholarship";
import storage from "../../store/storage";
import { hideSidebarState } from "../../store/states/settings";

const Topbar = () => {
  const [scholarships, setScholarshipList] = React.useState([]);
  const [scholarship, setScholarship] = useRecoilState(currentScholarshipState);
  const profileMenuItems = [
    { title: "Аккаут", icon: "profile" },
    { title: "Выйти", icon: "logout" },
  ];
  const user = authApi.getLocalUser();
  const [isSideHidden, setSidebarState] = useRecoilState(hideSidebarState);

  React.useEffect(() => {
    scholarshipApi.getAll().then((res) => {
      if (!res.error) {
        setScholarshipList(res.data);
      }
    });
  }, []);

  React.useLayoutEffect(() => {
    if (scholarships.length > 0 && scholarship == null) {
      handlerScholarship(scholarships[scholarships.length - 1]);
    }
  });

  function handlerScholarship(item) {
    setScholarship(item);
    storage.set("scholarship", item);
  }

  function handlerProfileMenu(item) {
    if (item.index === 0) {
      navigate("/settings");
    }
    if (item.index === 1) {
      authApi.logout().then((res) => {
        if (!res.error) {
          navigate("/account/login");
        }
      });
    }
  }

  function toggleSidebar() {
    setSidebarState(!isSideHidden);
  }

  return (
    <div className="topbar">
      <div className="tp-brand">
        <div className="toggle-sidebar">
          <button className="btn-none rounded" onClick={toggleSidebar}>
            <Icon name="hamburger-icon" />
          </button>
        </div>
        <Dropdown
          items={scholarships}
          onSelected={handlerScholarship}
          icon="graduate"
        >
          <span className="tp-brand-tag">
            {scholarship && (
              <span
                className="mr-4"
                style={{ color: "rgba(0, 0, 0, 0.6)", fontWeight: "500" }}
              >
                {scholarship.title}
              </span>
            )}
            <Icon name="chevron_down" color="rgba(0, 0, 0, 0.6)" size="12" />
          </span>
        </Dropdown>
      </div>
      <nav className="tp-nav">
        <ul className="ul-nav">
          <li>
            <Dropdown items={profileMenuItems} onSelected={handlerProfileMenu}>
              <a href="/#">
                <Crop color="rgba(0, 0, 0, 0.35)" size="30">
                  <Icon name="user" color="#fff" />
                </Crop>
                {user && (
                  <span
                    style={{ marginLeft: "2px", color: "rgba(0, 0, 0, 0.7)" }}
                  >
                    {user.full_name ? user.full_name : user.username}
                  </span>
                )}
                {/* <Icon name="chevron_down" color="rgba(0, 0, 0, 0.6)" size="8" /> */}
              </a>
            </Dropdown>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Topbar;
