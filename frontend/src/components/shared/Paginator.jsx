import React from "react";
import styled from "styled-components";
import { useState } from "react";

const Wrapper = styled.div`
  display: inline-block;
  padding: 2px;
  margin: 2px;

  .page-item {
    text-decoration: none;
    color: ${(props) => props.defaultTextColor};
    float: left;
    border: 0;
    padding: 5px 10px;
    margin: 2px;
    border-radius: ${(props) => props.borderRadius};
    transition: background-color 0.5s;
    &.active {
      background-color: ${(props) => props.defaultActiveColor};
      color: ${(props) => props.defaultActiveTextColor};
    }

    &:hover:not(.active) {
      background-color: ${(props) => props.defaultHighlightColor};
      color: ${(props) => props.defaultTextHighlightColor};
    }
  }
`;

const Paginator = ({
  totalLines,
  linesPerPage,
  activeColor,
  activeTextColor,
  textColor,
  highlightColor,
  highlightTextColor,
  paginate,
  rounded = false,
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const pages = [];

  const defaultTextColor = textColor ? textColor : "rebeccapurple";
  const defaultActiveColor = activeColor ? activeColor : defaultTextColor;
  const defaultActiveTextColor = activeTextColor ? activeTextColor : "#fff";
  const defaultHighlightColor = highlightColor ? highlightColor : "lightgray";
  const defaultTextHighlightColor = highlightTextColor
    ? highlightTextColor
    : defaultTextColor;
  const borderRadius = rounded ? "50%" : "5px";

  for (let i = 1; i < Math.ceil(totalLines / linesPerPage); i++) {
    pages.push(i);
  }

  function handlerPaginate(page) {
    paginate(page);
    setCurrentPage(page);
  }

  function prevPage() {
    if (currentPage > 1) {
      paginate(currentPage - 1);
      setCurrentPage(currentPage - 1);
    }
  }
  function nextPage() {
    if (currentPage < pages.length) {
      paginate(currentPage + 1);
      setCurrentPage(currentPage + 1);
    }
  }

  return (
    <Wrapper
      defaultTextColor={defaultTextColor}
      borderRadius={borderRadius}
      defaultActiveColor={defaultActiveColor}
      defaultActiveTextColor={defaultActiveTextColor}
      defaultHighlightColor={defaultHighlightColor}
      defaultTextHighlightColor={defaultTextHighlightColor}
    >
      <div className="paginate">
        {currentPage > 1 && (
          <button className="btn page-item" onClick={prevPage}>
            «
          </button>
        )}
        {pages.map((page) => (
          <button
            className={`btn page-item  ${currentPage === page ? "active" : ""}`}
            key={page}
            onClick={() => handlerPaginate(page)}
          >
            {page}
          </button>
        ))}

        {currentPage < pages.length && (
          <button className="btn page-item" onClick={nextPage}>
            »
          </button>
        )}
      </div>
    </Wrapper>
  );
};

export default Paginator;
