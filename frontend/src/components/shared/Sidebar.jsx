import React from "react";
import { useRecoilValue } from "recoil";
import logo from "../../assets/svg/logo.svg";
import { hideSidebarState } from "../../store/states/settings";
import NavLink from "./NavLink";

export default () => {
  const hideSidebar = useRecoilValue(hideSidebarState);

  return (
    <div className={`sidebar ${hideSidebar ? "hidden" : "show"}`}>
      <div className="sb-content">
        <div className="sb-header">
          <NavLink to="/">
            <img
              className="sb-header-logo"
              src={logo}
              alt="logo"
              width="130"
              height="130"
            />
          </NavLink>
          <div className="sb-header-title">Международный</div>
          <div className="sb-header-title">Отдел</div>
        </div>
        <ul className="sb-menu">
          <li>
            <NavLink to="/" exact>
              Главная
            </NavLink>
          </li>
          <li>
            <NavLink to="/student">Студенты</NavLink>
          </li>
          <li>
            <NavLink to="/visa">Визы</NavLink>
          </li>
          <li>
            <NavLink to="/domitory">Общежития</NavLink>
          </li>
          <li>
            <NavLink to="/settings">Настройки</NavLink>
          </li>
          <li>
            <NavLink to="/devtools">DevTools</NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
};
