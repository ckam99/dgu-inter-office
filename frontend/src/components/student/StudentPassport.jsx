import React from "react";

export default ({ passport }) => {
  return (
    <div className="panel">
      <div className="panel-title">Паспортные данные</div>
      <div className="panel-body">
        {passport && (
          <ul className="key-value-item">
            <li>
              <span style={{ color: "#555" }} className="bold">
                Номер:
              </span>
              <span className="ml-2">{passport.number}</span>
            </li>
            <li>
              <span style={{ color: "#555" }} className="bold">
                Дата выдачи:
              </span>
              <span className="ml-2">{passport.date_of_issue}</span>
            </li>
            <li>
              <span style={{ color: "#555" }} className="bold">
                Срок действия:
              </span>
              <span className="ml-2">{passport.expiry_at}</span>
            </li>
            <li>
              <span style={{ color: "#555" }} className="bold">
                Выдан:
              </span>
              <span className="ml-2">{passport.delivery_place}</span>
            </li>
            <li>
              <span style={{ color: "#555" }} className="bold">
                Дата рождения:
              </span>
              <span className="ml-2">{passport.student.birthday}</span>
            </li>
          </ul>
        )}
      </div>
    </div>
  );
};
