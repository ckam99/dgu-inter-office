import React from "react";
import IconButton from "../../components/elements/IconButton";
import Flag from "react-country-flag";
import image from "../../assets/img/student.png";

export default () => {
  return (
    <div className="row">
      <div className="flex flex-align-center flex-justify-content-space-between">
        <div className="flex flex-align-center">
          <IconButton icon="arrow_left" size="48" />
          <img
            src={image}
            alt="photo"
            className="avatar"
            width="98"
            height="98"
          />
          <div className="flex flex-column">
            <span style={{ fontSize: "24px" }}>Бало Кристин</span>
            <div className="flex flex-center mt-1">
              <Flag
                countryCode="CI"
                svg
                style={{
                  width: "2em",
                  height: "2em",
                  marginRight: "3px",
                }}
              />
              <span style={{ color: "#828282" }} className="smart-text ml-2">
                Cote d'Ivoire
              </span>
            </div>
          </div>
        </div>
        <div className="flex">
          <IconButton icon="edit" size="48" color="#59218c" border="1" />
          <IconButton icon="remove" size="48" border="1" />
        </div>
      </div>
    </div>
  );
};
