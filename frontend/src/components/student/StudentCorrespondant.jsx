import React from "react";
import { truncate } from "../../helpers/strings";

export default ({ correspondants = [] }) => {
  return (
    <div className="panel">
      <div className="panel-title">Корреспонденты</div>
      <div className="panel-body">
        <table className="table no-hover">
          <thead>
            <tr>
              <th>Фамилия</th>
              <th>Тип отношения</th>
              <th>Дата рождения</th>
              <th>Контакт</th>
              <th>Адрес</th>
            </tr>
          </thead>
          <tbody>
            {correspondants.map((correspondant, k) => (
              <tr key={k}>
                <td title={correspondant.name}>
                  {truncate(correspondant.name, 25)}
                </td>
                <td>{correspondant.type}</td>
                <td>{correspondant.birthday}</td>
                <td>{correspondant.phone}</td>
                <td title={correspondant.address}>
                  {truncate(correspondant.address, 20)}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
