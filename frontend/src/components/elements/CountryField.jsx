import React from "react";
import Flag from "react-country-flag";
import { truncate } from "../../helpers/strings";

const CountryField = ({ country, width, onClick }) => {
  return (
    <div
      style={{
        border: "1px solid #ccc",
        padding: "5px",
        margin: "5px 1px",
        background: "#fff",
        width: `${width}px` || "100%",
        minHeight: "40px",
        display: "flex",
        alignItems: "center",
      }}
      onClick={() => onClick()}
    >
      {!country && <span className="placeholder">Select country</span>}
      {country && (
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Flag
            countryCode={country.iso2}
            svg
            style={{
              width: "1em",
              height: "1em",
              marginRight: "5px",
            }}
          />
          <span style={{ fontSize: "12px", color: "rgba(35, 35, 35, 0.75)" }}>
            {truncate(country.name, 28)}
          </span>
        </div>
      )}
    </div>
  );
};

export default CountryField;
