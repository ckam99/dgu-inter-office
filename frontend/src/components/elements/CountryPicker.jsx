import React, { useRef, useImperativeHandle, useState, useEffect } from "react";
import Popup from "../shared/Popup";
import Country from "../../api/country";
import Flag from "react-country-flag";
import SearchField from "./SearchField";
import { truncate } from "../../helpers/strings";

const CountryPicker = ({ refId, onSelected }) => {
  const popup = useRef(null);
  const [searchValue, setSearchValue] = useState("");
  const [selected, setSelected] = useState(null);
  const [countries, setCountries] = useState([]);

  const filteredCountries = countries.filter((c) => {
    return c.name.toLowerCase().match(searchValue.toLowerCase());
  });

  useEffect(() => {
    Country.getAll().then((response) => {
      if (!response.error) {
        setCountries(response.data);
      }
    });
  }, []);

  useImperativeHandle(refId, () => ({
    show() {
      popup.current.open();
    },
    close() {
      popup.current.close();
    },
    selected() {
      return selected;
    },
  }));
  function close() {
    popup.current.close();
    onSelected(selected);
  }
  function onSearchChange(s) {
    setSearchValue(s);
  }

  function handlerSelect(e) {
    // popup.current.close();
    // onSelected(e);
    setSelected(e);
  }

  return (
    <Popup title="Pick country" refId={popup} width="400">
      <div style={{}}>
        <SearchField
          placeholder="Поиск"
          value={searchValue}
          onChange={onSearchChange}
        />
        <div
          style={{
            height: "400px",
            overflow: "hidden",
            overflowY: "auto",
            marginTop: "10px",
          }}
        >
          {filteredCountries.map((country, k) => {
            return (
              <div
                className={selected === country ? "selected" : ""}
                key={k}
                style={{
                  padding: "10px 0",
                  borderBottom: "1px solid #eee",
                  display: "flex",
                  alignItems: "center",
                  cursor: "pointer",
                }}
                onClick={() => handlerSelect(country)}
                title={country.name}
              >
                <Flag
                  countryCode={country.iso2}
                  svg
                  style={{
                    width: "2em",
                    height: "2em",
                    marginRight: "5px",
                  }}
                />
                <span style={{ fontSize: "20px" }}>
                  {truncate(country.name, 28)}
                </span>
              </div>
            );
          })}
        </div>
        {selected && (
          <div
            style={{
              position: "absolute",
              left: "0",
              right: "0",
              bottom: "0",
              padding: "5px 15px",
              background: "#f5f5f5",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Flag
                countryCode={selected.iso2}
                svg
                style={{
                  width: "1em",
                  height: "1em",
                  marginRight: "5px",
                }}
              />
              <span style={{ fontSize: "12px" }}>{selected.name}</span>
            </div>

            <button onClick={close}>Select</button>
          </div>
        )}
      </div>
    </Popup>
  );
};

export default CountryPicker;
