import React, { useState } from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";

const Dropdown = ({ children, items, refId, icon, onSelected }) => {
  const [isActive, setIsActive] = useState(false);

  return (
    <div className="dropdown" ref={refId}>
      <div className={`dropdown__header ${isActive ? "is-active" : ""}`}>
        {children}
        <input
          type="text"
          className="checkmark"
          readOnly
          onFocus={() => {
            setIsActive(true);
          }}
          onBlur={() => {
            setIsActive(false);
          }}
        />
      </div>
      <ul className="dropdown__content">
        {items.map((item, k) => {
          return (
            <li onClick={() => onSelected({ ...item, index: k })} key={k}>
              <section className="dropdown__content__item">
                {icon && <Icon name={icon} color="rgba(0, 0, 0, 0.3)" />}
                {item.icon && (
                  <Icon name={item.icon} color="rgba(0, 0, 0, 0.8)" size="18" />
                )}
                <span> {item.title}</span>
              </section>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

Dropdown.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Dropdown;
