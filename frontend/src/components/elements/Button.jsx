import React from "react";
import loader from "../../assets/gif/loader.gif";

export default ({ onClick, loading, type, children }) => {
  return (
    <button
      type={type}
      className="flat-btn"
      onClick={onClick}
      style={{
        display: "flex",
        background: "#59218c",
        alignItems: "center",
        justifyContent: "center",
        color: "#fff",
      }}
      disabled={loading}
    >
      {loading && (
        <img src={loader} alt="dfs" style={{ marginRight: "7px" }} width="24" />
      )}
      {!loading && children}
    </button>
  );
};
