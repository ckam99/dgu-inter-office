import React from "react";
import iconUri from "../../assets/svg/icons.svg";

export default ({ icon, color, border, size = 32 }) => {
  const iconSize = `${size - size / 2}px`;
  const bor = border ? `${border}px solid ${color}` : 0;
  return (
    <button
      className="icon-button"
      style={{
        width: `${size}px`,
        height: `${size}px`,
        border: bor,
      }}
    >
      <svg
        className="ib-icon"
        width={iconSize}
        height={iconSize}
        fill={color || "#555"}
      >
        <use xlinkHref={`${iconUri}#${icon}`} />
      </svg>
    </button>
  );
};
