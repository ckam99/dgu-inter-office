import React, { Fragment } from "react";

export default ({ children }) => {
  return (
    <div className="area-auth">
      <div className="overlay overlay-1"></div>
      <div className="overlay overlay-2"></div>
      <Fragment>{children}</Fragment>
    </div>
  );
};
