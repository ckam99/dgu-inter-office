import os
from faker import Faker
from core.shortcuts import write

faker = Faker('ru_RU')
app_name = 'domitory'


def manager(model, lines=5):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['name'] = faker.name()
        data["fields"]['address'] = faker.address()
        data["fields"]['phone'] = faker.phone_number()
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)


def bedroom(model, lines=100):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['beds'] = faker.random_int(min=1, max=5, step=1)
        data["fields"]['floor'] = faker.random_int(min=1, max=12, step=1)
        data["fields"]['domitory'] = faker.random_int(min=1, max=2, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)
