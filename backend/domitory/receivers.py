from django.db.models import signals
from django.dispatch import receiver
from .models import Domitory, Bedroom


@receiver(signals.pre_save, sender=Domitory)
def pre_save_domitory(sender, instance=None, *args, **kwargs):
    print('******** pre_save_domitory receiver has been called ***********')


@receiver(signals.pre_save, sender=Bedroom)
def pre_save_bedroom(sender, instance=None, *args, **kwargs):
    pr = "0" if instance.beds < 10 else ""
    instance.number = f"{instance.floor}{pr}{instance.beds}"


@receiver(signals.post_save, sender=Domitory)
def post_save_domitory(sender, instance=None, created=False, **kwargs):
    print('******** post_save_domitory(receiver has been called ***********')
