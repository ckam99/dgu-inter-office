from django.apps import AppConfig
from core.shortcuts import load_module


class DomitoryConfig(AppConfig):
    name = 'domitory'

    def ready(self):
        load_module('{}.receivers'.format(self.name))
