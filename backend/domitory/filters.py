import django_filters
from . import models


class ManagerFilter(django_filters.FilterSet):
    class Meta:
        model = models.Manager
        fields = ['name', 'phone']


class DomitoryFilter(django_filters.FilterSet):
    class Meta:
        model = models.Domitory
        fields = ['title', 'manager', 'phone']


class BedroomFilter(django_filters.FilterSet):
    class Meta:
        model = models.Bedroom
        fields = ['beds', 'floor', 'number', 'domitory']
