from django.db import models
from django.utils import timezone


class Manager(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.name


class Domitory(models.Model):
    class Meta:
        verbose_name = 'domitory'
        verbose_name_plural = 'domitories'

    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50)
    address = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    manager = models.ForeignKey(Manager, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.title


class Bedroom(models.Model):
    id = models.AutoField(primary_key=True)
    beds = models.IntegerField(default=1)
    floor = models.IntegerField(default=1)
    number = models.CharField(max_length=10, null=True, blank=True)
    domitory = models.ForeignKey(Domitory, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return '{0} - {1}'.format(self.domitory.title, self.number)
