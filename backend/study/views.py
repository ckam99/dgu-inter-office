from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from .models import Scholarship, Degree, Department, Program
from .serializers import (
    ScholarshipSerializer, DegreeSerializer, DepartmentSerializer,
    ProgramSerializer)
from . import filters


class ScholarshipViewSet(viewsets.ModelViewSet):
    queryset = Scholarship.objects.all()
    serializer_class = ScholarshipSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.ScholarshipFilter


class DegreeViewSet(viewsets.ModelViewSet):
    queryset = Degree.objects.all()
    serializer_class = DegreeSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.DegreeFilter


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.DepartmentFilter


class ProgramViewSet(viewsets.ModelViewSet):
    queryset = Program.objects.all()
    serializer_class = ProgramSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.ProgramFilter
