from rest_framework import serializers
from .models import Scholarship, Department, Program, Degree
from core.shortcuts import PresentablePrimaryKeyRelatedField


class ScholarshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scholarship
        fields = '__all__'
        read_only_fields = ('title',)


class DegreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Degree
        fields = '__all__'


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = Program
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['department'] = PresentablePrimaryKeyRelatedField(
            label='Department', presentation_serializer=DepartmentSerializer,
            queryset=Department.objects.all(),
        )
