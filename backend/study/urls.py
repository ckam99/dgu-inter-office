from rest_framework.routers import DefaultRouter
from study import views

router = DefaultRouter()

router.register(r'scholarships', views.ScholarshipViewSet)
router.register(r'degrees', views.DegreeViewSet)
router.register(r'departments', views.DepartmentViewSet)
router.register(r'programs', views.ProgramViewSet)


app_name = 'study'

urlpatterns = router.urls
