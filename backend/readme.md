## Django app

### Prerequis

##### Migrate database

`python manage.py migrate`

##### Create database migration

`python manage.py makemigrations`

##### Load list countries

`python manage.py loaddata countries.json`

##### Run localy server

`python manage.py runserver`

### Help

##### Create super user

`python manage.py createsuperuser`

##### Show migrations

`python manage.py showmigrations`

##### Clear the migration history :

`python manage.py migrate --fake [app name] zero`

#### Fake the initial migration

`python manage.py migrate --fake-initial`

##### Export data from django database

`python manage.py dumpdata [app_label.ModelName] --indent 2 --format json > fixtures/[filename].json`

##### Import data from django database

`python manage.py loaddata [filename].json`

##### Load fixtures for development database

```bash

python manage.py loaddata countries.json account.json  scholarship.json  degree.json department.json  program.json manager.json domitory.json bedroom.json student.json passport.json visa.json correspondant.json acomodation.json
# optional
python manage.py loaddata register.json

```
