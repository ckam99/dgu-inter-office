from rest_framework import viewsets
from django_filters import rest_framework as filters
from django.shortcuts import get_object_or_404
from rest_framework.filters import (SearchFilter, OrderingFilter)
from rest_framework.decorators import action
from rest_framework.response import Response
from . import models, serializers
from . import filters as local_filters
from core.shortcuts import get_int_or_404


# from .paginators import StudentListPagination


class CountryViewSet(viewsets.ModelViewSet):
    queryset = models.Country.objects.all()
    serializer_class = serializers.CountrySerializer
    filter_backends = [filters.DjangoFilterBackend, OrderingFilter]
    filterset_class = local_filters.CountryFilter

    def get_queryset(self):
        queryset = models.Country.objects.all()
        limit = self.request.query_params.get('limit', None)
        offset = self.request.query_params.get('offset', None)
        if limit is not None and offset is not None:
            queryset = queryset[get_int_or_404(
                offset):get_int_or_404(limit)]
        return queryset


class StudentViewSet(viewsets.ModelViewSet):
    queryset = models.Student.objects.all()
    serializer_class = serializers.StudentSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter,)
    filterset_class = local_filters.StudentFilter

    # pagination_class = StudentListPagination

    def get_queryset(self):
        queryset = models.Student.objects.all()
        limit = self.request.query_params.get('limit', None)
        offset = self.request.query_params.get('offset', None)
        if limit is not None and offset is not None:
            queryset = queryset[get_int_or_404(
                offset):get_int_or_404(limit)]
        return queryset

    @action(detail=False, methods=['get'])
    def recent_registers(self, request):
        limit = request.query_params.get("limit", 10)
        offset = request.query_params.get("offset", 0)
        scholarship = request.query_params.get("scholarship")
        name = request.query_params.get("name")

        queryset = models.Register.objects.all().order_by('student__name')

        if name is not None:
            queryset = queryset.filter(
                student__name__contains=name)

        if scholarship is not None:
            queryset = queryset.filter(scholarship=scholarship)

        data = {}
        data['count'] = queryset.count()

        if limit is not None and offset is not None:
            queryset = queryset[get_int_or_404(offset):get_int_or_404(limit)]
        serializer = serializers.RegisterSerializer(queryset, many=True)

        data['result'] = serializer.data
        return Response(data)

    # @action(detail=True, methods=['get'])
    # def get_registers(self, request, pk=None):
    #     student = get_object_or_404(models.Student, pk=pk)
    #     queryset = models.Register.objects.filter(student=student)

    #     scholarship = request.query_params.get("scholarship", None)
    #     if scholarship is not None:
    #         queryset = queryset.filter(scholarship=get_int_or_404(scholarship))

    #     serializer = serializers.RegisterSerializer(queryset, many=True)
    #     return Response(serializer.data)


class VisaViewSet(viewsets.ModelViewSet):
    queryset = models.Visa.objects.all()
    serializer_class = serializers.VisaSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter,)
    filterset_class = local_filters.VisaFilter


class PassportViewSet(viewsets.ModelViewSet):
    queryset = models.Passport.objects.all()
    serializer_class = serializers.PassportSerializer
    filter_backends = [filters.DjangoFilterBackend, OrderingFilter]
    filterset_class = local_filters.PassportFilter


class CorrespondantViewSet(viewsets.ModelViewSet):
    queryset = models.Correspondant.objects.all()
    serializer_class = serializers.CorrespondantSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter,)
    filterset_class = local_filters.CorrespondantFilter


class RegisterViewSet(viewsets.ModelViewSet):
    queryset = models.Register.objects.all()
    serializer_class = serializers.RegisterSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter,)
    filterset_class = local_filters.RegisterFilter

    def get_queryset(self):
        queryset = models.Register.objects.all().order_by('student__name')
        limit = self.request.query_params.get('limit', None)
        offset = self.request.query_params.get('offset', None)
        if limit is not None and offset is not None:
            queryset = queryset[get_int_or_404(
                offset):get_int_or_404(limit)]
        return queryset


class AcomodationViewSet(viewsets.ModelViewSet):
    queryset = models.Acomodation.objects.all()
    serializer_class = serializers.AcomodationSerializer
    filter_backends = (filters.DjangoFilterBackend,
                       SearchFilter, OrderingFilter)
    filterset_fields = ('student', 'scholarship',
                        'bedroom', 'paid', 'created_at')
    # filter_fields = ()
    # search_fields = ()
