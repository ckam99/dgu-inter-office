from django.apps import AppConfig
from core.shortcuts import load_module


class StudentConfig(AppConfig):
    name = 'student'

    def ready(self):
        load_module('{}.receivers'.format(self.name))
