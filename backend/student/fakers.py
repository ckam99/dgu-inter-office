import os
import random
from faker import Faker
from core.shortcuts import write

faker = Faker('ru_RU')
app_name = 'student'


def student(model, lines=10):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['name'] = faker.name()
        data["fields"]['birthday'] = faker.date()
        data["fields"]['placeofbirth'] = faker.city()
        data["fields"]['address'] = faker.address()
        data["fields"]['phone'] = faker.phone_number()
        data["fields"]['citizen'] = faker.random_int(min=1, max=250, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)


def correspondant(model, lines=2):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['name'] = faker.name()
        data["fields"]['birthday'] = faker.date()
        data["fields"]['type'] = random.choice(
            ["отец", "мать", "дядя", "тетя", "другой"])
        data["fields"]['address'] = faker.address()
        data["fields"]['phone'] = faker.phone_number()
        data["fields"]['student'] = faker.random_int(min=1, max=500, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)


def passport(model, lines=1):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['number'] = faker.license_plate()
        data["fields"]['date_of_issue'] = faker.date()
        data["fields"]['expiry_at'] = faker.date()
        data["fields"]['delivery_place'] = faker.city()
        data["fields"]['student'] = faker.random_int(min=1, max=500, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)


def visa(model, lines=2):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['number'] = faker.license_plate()
        data["fields"]['date_of_issue'] = faker.date()
        data["fields"]['expiry_at'] = faker.date()
        data["fields"]['delivery_place'] = faker.city()
        data["fields"]['student'] = faker.random_int(min=1, max=500, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)


def register(model, lines=2):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['state'] = random.choice([0, 1])
        data["fields"]['level'] = random.choice([1, 2, 3, 4])
        data["fields"]['student'] = faker.random_int(min=1, max=500, step=1)
        data["fields"]['scholarship'] = faker.random_int(min=1, max=3, step=1)
        data["fields"]['degree'] = faker.random_int(min=1, max=3, step=1)
        data["fields"]['program'] = faker.random_int(min=1, max=5, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)


def acomodation(model, lines=2):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['paid'] = random.choice([0, 1])
        data["fields"]['student'] = faker.random_int(min=1, max=500, step=1)
        data["fields"]['scholarship'] = faker.random_int(min=1, max=3, step=1)
        data["fields"]['bedroom'] = faker.random_int(min=1, max=500, step=1)
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)
