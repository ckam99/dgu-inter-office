from rest_framework import serializers
from .models import (Student, Visa, Register, Passport,
                     Correspondant, Acomodation, Country)
from study.serializers import (
    ScholarshipSerializer, DegreeSerializer, ProgramSerializer)
from study.models import Scholarship, Degree, Program
from domitory.models import Bedroom
from domitory.serializers import BedroomSerializer
from core.shortcuts import PresentablePrimaryKeyRelatedField


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'
        extra_kwargs = {
            'name': {'read_only': True},
            'name_ru': {'read_only': True},
            'iso': {'read_only': True},
            'iso2': {'read_only': True},
            'e164': {'read_only': True},
        }


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['citizen'] = PresentablePrimaryKeyRelatedField(
            label='Citizen', presentation_serializer=CountrySerializer,
            queryset=Country.objects.all(),
        )


class VisaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visa
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['student'] = PresentablePrimaryKeyRelatedField(
            label='Student', presentation_serializer=StudentSerializer,
            queryset=Student.objects.all(),
        )


class RegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Register
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['scholarship'] = PresentablePrimaryKeyRelatedField(
            label='Scholarship', presentation_serializer=ScholarshipSerializer,
            queryset=Scholarship.objects.all(),
        )
        self.fields['student'] = PresentablePrimaryKeyRelatedField(
            label='Student', presentation_serializer=StudentSerializer,
            queryset=Student.objects.all(),
        )
        self.fields['degree'] = PresentablePrimaryKeyRelatedField(
            label='Degree', presentation_serializer=DegreeSerializer,
            queryset=Degree.objects.all(),
        )
        self.fields['program'] = PresentablePrimaryKeyRelatedField(
            label='Program', presentation_serializer=ProgramSerializer,
            queryset=Program.objects.all(),
        )


class PassportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passport
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['student'] = PresentablePrimaryKeyRelatedField(
            label='Student', presentation_serializer=StudentSerializer,
            queryset=Student.objects.all(),
        )


class CorrespondantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Correspondant
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['student'] = PresentablePrimaryKeyRelatedField(
            label='Student', presentation_serializer=StudentSerializer,
            queryset=Student.objects.all(),
        )


class AcomodationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Acomodation
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['scholarship'] = PresentablePrimaryKeyRelatedField(
            label='Scholarship', presentation_serializer=ScholarshipSerializer,
            queryset=Scholarship.objects.all(),
        )
        self.fields['student'] = PresentablePrimaryKeyRelatedField(
            label='Student', presentation_serializer=StudentSerializer,
            queryset=Student.objects.all(),
        )
        self.fields['bedroom'] = PresentablePrimaryKeyRelatedField(
            label='Bedroom', presentation_serializer=BedroomSerializer,
            queryset=Bedroom.objects.all(),
        )
