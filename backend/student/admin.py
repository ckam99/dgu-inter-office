from django.contrib import admin
from .models import (Student, Visa, Register, Passport,
                     Correspondant, Acomodation, Country)


class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'iso2', 'iso', 'e164',)
    search_fields = ('name', 'iso2',)
    readonly_fields = ()

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()


class StudentAdmin(admin.ModelAdmin):

    ordering = ['name']
    list_display = ('id', 'name', 'citizen',  'birthday', 'placeofbirth',
                    'address', 'phone', 'avatar', 'citizen')
    search_fields = ('name', 'placeofbirth', 'phone', 'address')
    readonly_fields = ()

    filter_horizontal = ()
    list_filter = ('citizen',)
    fieldsets = ()


class RegisterAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ('id', 'scholarship', 'student',  'degree', 'program',)
    search_fields = ('scholarship', 'student',)
    readonly_fields = ()

    filter_horizontal = ()
    list_filter = ('student', 'scholarship',)
    fieldsets = ()


admin.site.register(Acomodation)
admin.site.register(Correspondant)
admin.site.register(Passport)
admin.site.register(Register, RegisterAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Visa)
admin.site.register(Country, CountryAdmin)
