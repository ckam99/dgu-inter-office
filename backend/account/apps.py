from django.apps import AppConfig
from core.shortcuts import load_module


class AccountConfig(AppConfig):
    name = 'account'
    verbose_name = 'Account'

    def ready(self):
        super().ready()
        load_module('{}.receivers'.format(self.name))
