from rest_framework import serializers
from .models import Account
from django.contrib.auth import authenticate
from django.core import exceptions
from django.shortcuts import get_object_or_404
from core.shortcuts import validateEmail, validatePassword, get_password_errors, contains


class AccountListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'full_name', 'email', 'username',
                  'is_admin', 'is_active', 'date_joined', 'last_login']


class AccountRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'full_name', 'email', 'username', 'is_admin',
                  'is_active', 'date_joined', 'last_login']
        # action_fields = {
        #     'list': {'fields': ('full_name', 'is_admin', 'is_active')}
        # }

        extra_kwargs = {
            'email': {'read_only': True},
            'username': {'read_only': True},
        }


class AccountUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'full_name', 'email', 'username', 'is_admin',
                  'is_active', 'date_joined', 'last_login']
        extra_kwargs = {
            'email': {'read_only': True},
            'username': {'read_only': True},
        }


class AccountRegisterSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(
        style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = Account
        fields = ['full_name', 'email', 'username',
                  'password', 'password2', 'is_admin', 'is_active']
        extra_kwargs = {
            'password': {'write_only': True, }
        }

    def save(self):
        account = Account(
            full_name=self.validated_data['full_name'],
            is_admin=self.validated_data['is_admin'],
            is_active=self.validated_data['is_active'],
            email=self.validated_data['email'],
            username=self.validated_data['username'],
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError(
                {'password': 'Passwords must match.'})

        if not validatePassword(password):
            raise serializers.ValidationError(
                {'password': get_password_errors()})
        
        if contains(account.username, password):
            raise serializers.ValidationError(
                {'password': "The password is too similar to the username."})

        account.set_password(password)
        account.save()
        return account


class AccountAuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            # Check if user sent email
            if not validateEmail(email):
                raise exceptions.ValidationError(
                    {'email': "Email is not valid"}
                )
            user = authenticate(username=email, password=password)

            if user:
                if not user.is_active:
                    msg = {'error': 'User account is disabled.'}
                    raise exceptions.ValidationError(msg)
            else:
                msg = {
                    'error': 'Unable to authenticate in with provided credentials.'
                }
                raise exceptions.ValidationError(msg)
        else:
            msg = {'error': 'Must include "email" and "password"'}
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs


class AccountResetPasswordSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(
        style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = Account
        fields = ['password', 'password2']
        extra_kwargs = {
            'password': {'write_only': True, },
        }

    def save(self, pk):
        account = get_object_or_404(Account, pk=pk)
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError(
                {'password': 'Passwords must match.'})
        account.set_password(password)
        account.save()
        return account
