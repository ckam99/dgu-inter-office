import os
import random
from faker import Faker
from core.shortcuts import write

faker = Faker('ru_RU')
app_name = 'account'


def account(model, lines=10):
    dic = []
    for i in range(0, lines):
        data = {}
        data["model"] = f"{app_name}.{str(model).split('.')[1]}"
        data["fields"] = {}
        data["fields"]['full_name'] = faker.name()
        data["fields"]['email'] = faker.email(domain='dgu.ru')
        data["fields"]['username'] = faker.user_name()
        data["fields"]['password'] = 'aaa000AAA'
        data["fields"]['is_admin'] = random.choice([0, 1])
        data["fields"]['is_active'] = random.choice([0, 1])
        dic.append(data)
    path = os.path.join(os.getcwd(), app_name, 'fixtures',
                        f"{str(model).split('.')[1]}.json")
    write(path, dic)
