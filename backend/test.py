
from faker import Faker
import json            # To create a json file
from random import randint      # For student id
fake = Faker()


def input_data(model, x):

    # dictionary
    student_data = []
    for i in range(0, x):
        data = {}
        data["model"] = "student.students"
        data["field"] = {}
        data["field"]['id'] = randint(1, 100)
        data["field"]['name'] = fake.name()
        data["field"]['address'] = fake.address()
        data["field"]['latitude'] = str(fake.latitude())
        data["field"]['longitude'] = str(fake.longitude())
        student_data.append(data)
    print(student_data)

    # dictionary dumped as json in a json file
    with open('students.json', 'w') as fp:
        json.dump(student_data, fp)


input_data(10)
