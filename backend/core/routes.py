from rest_framework.routers import DefaultRouter
from study import views as studies
from domitory import views as domitories
from student import views as students
from account import views as accounts

router = DefaultRouter()

# account
router.register('accounts', accounts.AccountViewSet)

# study
router.register(r'scholarships', studies.ScholarshipViewSet)
router.register(r'degrees', studies.DegreeViewSet)
router.register(r'departments', studies.DepartmentViewSet)
router.register(r'programs', studies.ProgramViewSet)

# domitory
router.register(r'managers', domitories.ManagerViewSet)
router.register(r'domitories', domitories.DomitoryViewSet)
router.register(r'bedrooms', domitories.BedroomViewSet)

# student
router.register(r'students', students.StudentViewSet)
router.register(r'visas', students.VisaViewSet)
router.register(r'passports', students.PassportViewSet)
router.register(r'correspondants', students.CorrespondantViewSet)
router.register(r'registers', students.RegisterViewSet)
router.register(r'acommodations', students.AcomodationViewSet)
# countries
router.register('countries', students.CountryViewSet)
