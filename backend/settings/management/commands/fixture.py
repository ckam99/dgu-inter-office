from django.core.management.base import BaseCommand
import importlib


class Command(BaseCommand):
    help = 'Create randon fixtures datas'

    def add_arguments(self, parser):
        parser.add_argument('-l', '--lines', type=int,
                            help='Number of row to insert')
        parser.add_argument('-m', '--model', type=str, help='Target model', )

    def handle(self, *args, **kwargs):
        lines = kwargs['lines']
        model = str(kwargs['model']).lower()
        fun = getattr(
            importlib.import_module(f"{model.split('.')[0]}.fakers"),
            model.split('.')[1]
        )
        fun(model=model, lines=lines)
        self.stdout.write(
            f"Created {model.split('.')[1]}.json in fixtures with {lines} object(s)")
